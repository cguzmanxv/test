<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://netkiub.com
 * @since             1.0.0
 * @package           PagosOfflineVenezuela
 *
 * @wordpress-plugin
 * Plugin Name:       Pagos Offline Venezuela
 * Plugin URI:        https://netkiub.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0
 * Author:            Guillermo
 * Author URI:        https://netkiub.com
 * GitHub Plugin URI: https://github.com/guillermoscript/pagos-offline-venezuela
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pagos-offline-venezuela
 * Domain Path:       /languages
 */


require  dirname( __FILE__ ) . '/plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://e-commerce.apolloacbahamas.com/info.json',
	__FILE__, //Full path to the main plugin file or functions.php.
	'pagos-offline-venezuela'
);